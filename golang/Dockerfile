FROM golang:1.18 as base

WORKDIR /app

RUN useradd --user-group --create-home gopher

USER gopher

ENV GOPATH /go
ENV PATH $GOPATH/bin:/usr/local/go/bin:$PATH
# C compiler "gcc" not found (https://github.com/golang/go/issues/28065)
ENV CGO_ENABLED 0

COPY --chown=gopher:gopher go.* /app/

RUN go mod download

COPY --chown=gopher:gopher . .

FROM base as dev

RUN go install github.com/go-delve/delve/cmd/dlv@latest
RUN go install github.com/githubnemo/CompileDaemon@latest

RUN GOOS=linux go build -ldflags "-s -w" -o /go/bin/app ./cmd/app

FROM base as builder

RUN GOOS=linux go build -o /go/bin/app ./cmd/app

FROM alpine:latest as release

COPY --chown=nobody:nogroup --from=builder /go/bin/app /app

USER nobody

CMD ["/app"]
